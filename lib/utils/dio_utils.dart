import 'package:dio/dio.dart';
import 'package:dio_smart_retry/dio_smart_retry.dart';
import 'package:flutter/cupertino.dart';

class DioUtils {
  static late Dio _dio;

  static final BaseOptions _baseOptions = BaseOptions(
    headers: {},
    contentType: Headers.jsonContentType,
    responseType: ResponseType.json,
  );

  static final DioUtils _singleton = DioUtils._internal();

  /// dio init
  factory DioUtils() {
    //創建dio實例
    _dio = Dio(_baseOptions);

    _dio.interceptors
        .add(RetryInterceptor(dio: _dio, retries: 2, retryDelays: const [
      Duration(seconds: 1), // wait 1 sec before first retry
      Duration(seconds: 2), // wait 2 sec before second retry
    ]));
    return _singleton;
  }

  DioUtils._internal();

  /// get
  get(url, {queryParameters, options}) async {
    Response response;
    try {
      response = await _dio.get(url,
          queryParameters: queryParameters, options: options);
    } on DioError catch (error) {
      debugPrint('post error ---\n${error.response.toString()}');
      rethrow;
    }
    return response;
  }

  /// Post
  post(url, {data, queryParameters, options}) async {
    Response response;
    try {
      response = await _dio.post(url,
          data: data, queryParameters: queryParameters, options: options);
      debugPrint('post result ---${response.data}');
    } on DioError catch (error) {
      debugPrint('post error ---\n${error.response.toString()}');
      rethrow;
    }

    return response;
  }
}
