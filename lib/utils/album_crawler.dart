import 'package:album/common/model/album_model.dart';
import 'package:album/utils/dio_utils.dart';
import 'package:dio/dio.dart';

class AlbumCrawler {
  final DioUtils _dioUtils = DioUtils();

  // get Album from itunes by artist name
  Future<AlbumModel?> getAlbumByArtistName(String searchTerm) async {
    final url = "https://itunes.apple.com/search?term=$searchTerm&entity=album";
    final Response respond = await _dioUtils.get(url);

    if (respond.statusCode == 200) {
      // convert the json file into AlbumModel

      return albumModelFromJson(respond.data);
    } else {
      // return empty album list when get nothing
      return null;
    }
  }
}
