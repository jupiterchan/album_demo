import 'package:album/common/model/album_view_model.dart';
import 'package:flutter/material.dart';
import 'package:get_storage/get_storage.dart';

class Prefs {
  static const String boxKey = "album";
  static const String bookmarkAlbumKey = 'bookmarkAlbum';

  final _box = GetStorage(boxKey);

  AlbumViewModel get bookmarkAlbum {
    const emptyAlbum = AlbumViewModel(resultCount: 0, albums: []);
    try {
      final result = _box.read(bookmarkAlbumKey);
      return result == null ? emptyAlbum : AlbumViewModel.fromJson(result);
    } catch (e) {
      debugPrint(e.toString());
      return emptyAlbum;
    }
  }

  Future<void> saveBookmarkAlbum(AlbumViewModel bookmarkAlbum) async {
    try {
      await _box.write(bookmarkAlbumKey, bookmarkAlbum.toJson());
    } catch (e) {
      debugPrint(e.toString());
    }
  }
}
