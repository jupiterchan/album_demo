import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'logic.dart';

class AlbumDetailsPage extends StatelessWidget {
  final logic = Get.put(AlbumDetailsLogic());
  final state = Get.find<AlbumDetailsLogic>().state;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [
                  Colors.yellow.withOpacity(0.7),
                  Colors.transparent,
                  Colors.transparent,
                ]),
          ),
        ),
        Scaffold(
          backgroundColor: Colors.transparent,
          appBar: AppBar(
            backgroundColor: Colors.transparent,
            elevation: 0,
          ),
          body: SingleChildScrollView(
            child: Center(
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 64, vertical: 16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Row(
                      children: [
                        Expanded(
                          child: Hero(
                            tag: state.album.value.collectionId,
                            child: Image.network(
                              state.album.value.artworkUrl100,
                              fit: BoxFit.fill,
                            ),
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(height: 16),
                    Text(
                      state.album.value.collectionName,
                      style: const TextStyle(color: Colors.white, fontSize: 18),
                    ),
                    const SizedBox(height: 16),
                    Row(
                      children: [
                        Expanded(
                          child: Text(
                            state.album.value.artistName,
                            style: const TextStyle(
                                color: Colors.white70, fontSize: 16),
                          ),
                        ),
                        IconButton(
                          onPressed: logic.launchAlbumCollectionUrl,
                          icon: const Icon(
                            Icons.public,
                            color: Colors.white,
                          ),
                        ),
                        IconButton(
                          onPressed: () =>
                              logic.onBookmark(!state.album.value.bookmark),
                          icon: Obx(() {
                            return Icon(
                              state.album.value.bookmark
                                  ? Icons.favorite
                                  : Icons.favorite_border,
                              color: Colors.white,
                            );
                          }),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
