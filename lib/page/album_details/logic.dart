import 'package:album/page/home/logic.dart';
import 'package:get/get.dart';
import 'package:url_launcher/url_launcher.dart';

import 'state.dart';

class AlbumDetailsLogic extends GetxController {
  final AlbumDetailsState state = AlbumDetailsState();

  @override
  onInit() {
    super.onInit();
    state.album = Rx(Get.arguments);
  }

  onBookmark(bool bookmark) {
    state.album.value = state.album.value.copyWith(bookmark: bookmark);

    final homeLogic = Get.find<HomeLogic>();
    homeLogic.onBookmark(state.album.value);
  }

  launchAlbumCollectionUrl() {
    launchUrl(Uri.parse(state.album.value.collectionViewUrl));
  }
}
