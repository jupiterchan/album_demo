import 'package:album/common/model/album_model.dart';
import 'package:album/common/model/album_view_model.dart';
import 'package:album/utils/album_crawler.dart';
import 'package:album/utils/navigation.dart';
import 'package:album/utils/prefs.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

import 'state.dart';

class HomeLogic extends GetxController {
  final HomeState state = HomeState();

  @override
  void onInit() async {
    super.onInit();
    await _bookmarkAlbumInit();
    await _search();
  }

  // Search function in search bar to update the home screen album
  Future<void> onUpdateSearchTerm(String searchTerm) async {
    state.searchText.value = searchTerm;
    await _search();
  }

  void onBookmark(Album album) {
    if (album.bookmark) {
      _addBookmark(album.collectionId);
    } else {
      _removeBookmark(album.collectionId);
    }
  }

  void gotoAlbumDetailsPage(Album album) {
    Get.toNamed(Navigation.albumDetailsPage, arguments: album);
  }

  void onTapBottomNavigationBar(int index) {
    state.pageIndex.value = index;
  }

  ///Private Function-----------------------------------
  // search album in itunes
  Future<void> _search() async {
    state.isLoading.value = true;

    final searchedAlbum =
        await AlbumCrawler().getAlbumByArtistName(state.searchText.value);

    if (searchedAlbum != null) {
      final albumViewModel = _convertAlbumModelToAlbumViewModel(searchedAlbum);

      state.albumViewModel.value = albumViewModel;
    } else {
      // no result
    }

    state.isLoading.value = false;
  }

  // convert model into viewModel
  AlbumViewModel _convertAlbumModelToAlbumViewModel(AlbumModel albumModel) {
    return AlbumViewModel(
      resultCount: albumModel.resultCount,
      albums: albumModel.results.map<Album>(
        (album) {
          bool bookmark = _getBookmarkIndex(album.collectionId) != -1;

          return Album(
            collectionId: album.collectionId,
            artistName: album.artistName,
            collectionName: album.collectionName,
            artistViewUrl: album.artistViewUrl,
            collectionViewUrl: album.collectionViewUrl,
            artworkUrl100: album.artworkUrl100,
            collectionPrice: album.collectionPrice,
            bookmark: bookmark,
          );
        },
      ).toList(),
    );
  }

  // check the album is bookmarked
  int _getBookmarkIndex(int collectionId) {
    int index = state.bookmarkAlbumViewModel.value.albums
        .indexWhere((element) => element.collectionId == collectionId);
    return index;
  }

  int _getAlbumIndex(int collectionId) {
    int index = state.albumViewModel.value.albums
        .indexWhere((element) => element.collectionId == collectionId);
    return index;
  }

  void _addBookmark(int collectionId) {
    int index = _getAlbumIndex(collectionId);

    // set bookmark in home page list
    final tempAlbumList = state.albumViewModel.value.albums.toList();
    tempAlbumList[index] = tempAlbumList[index].copyWith(bookmark: true);
    state.albumViewModel.value =
        state.albumViewModel.value.copyWith(albums: tempAlbumList);

    // add album into bookmark list
    final tempBookmarkAlbumList =
        state.bookmarkAlbumViewModel.value.albums.toList();
    tempBookmarkAlbumList.add(state.albumViewModel.value.albums[index]);
    state.bookmarkAlbumViewModel.value = state.bookmarkAlbumViewModel.value
        .copyWith(
            albums: tempBookmarkAlbumList,
            resultCount: state.bookmarkAlbumViewModel.value.resultCount + 1);
  }

  void _removeBookmark(int collectionId) {
    int index = _getAlbumIndex(collectionId);

    // remove album from bookmark list
    final tempBookmarkAlbumList =
        state.bookmarkAlbumViewModel.value.albums.toList();
    tempBookmarkAlbumList.remove(state.albumViewModel.value.albums[index]);
    state.bookmarkAlbumViewModel.value = state.bookmarkAlbumViewModel.value
        .copyWith(
            albums: tempBookmarkAlbumList,
            resultCount: state.bookmarkAlbumViewModel.value.resultCount - 1);

    // set bookmark in home page list
    final tempAlbumList = state.albumViewModel.value.albums.toList();
    tempAlbumList[index] = tempAlbumList[index].copyWith(bookmark: false);
    state.albumViewModel.value =
        state.albumViewModel.value.copyWith(albums: tempAlbumList);
  }

  Future<void> _bookmarkAlbumInit() async {
    Prefs prefs = Prefs();
    // get bookmark albums from storage
    state.bookmarkAlbumViewModel.value = prefs.bookmarkAlbum;
    // save bookmark albums when every data change
    state.bookmarkAlbumViewModel.listen((data) async {
      await prefs.saveBookmarkAlbum(data);
    });
  }
}
