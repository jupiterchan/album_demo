import 'package:album/common/model/album_view_model.dart';
import 'package:album/page/home/widget/album_grid_tile.dart';
import 'package:flutter/material.dart';

class AlbumGridView extends StatelessWidget {
  const AlbumGridView(
      {Key? key,
      required this.albumViewModel,
      required this.onBookmark,
      required this.gotoAlbumDetailsPage})
      : super(key: key);
  final AlbumViewModel albumViewModel;
  final Function(Album) onBookmark;
  final Function(Album) gotoAlbumDetailsPage;

  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      padding: const EdgeInsets.all(16),
      itemCount: albumViewModel.resultCount,
      gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          childAspectRatio: 0.8,
          mainAxisSpacing: 16,
          crossAxisSpacing: 16),
      itemBuilder: (context, index) => AlbumGridTile(
        album: albumViewModel.albums[index],
        onBookmark: onBookmark,
        gotoAlbumDetailsPage: gotoAlbumDetailsPage,
      ),
    );
  }
}
