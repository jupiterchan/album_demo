import 'package:album/common/model/album_view_model.dart';
import 'package:flutter/material.dart';

class AlbumGridTile extends StatelessWidget {
  const AlbumGridTile(
      {Key? key,
      required this.album,
      required this.onBookmark,
      required this.gotoAlbumDetailsPage})
      : super(key: key);
  final Album album;
  final Function(Album) onBookmark;
  final Function(Album) gotoAlbumDetailsPage;

  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: () => gotoAlbumDetailsPage(album),
      tileColor: Colors.transparent,
      selectedTileColor: Colors.grey,
      title: Hero(
        tag: album.collectionId,
        child: Image.network(
          album.artworkUrl100,
          fit: BoxFit.fill,
        ),
      ),
      subtitle: Padding(
        padding: const EdgeInsets.only(top: 8.0),
        child: Text(
          album.collectionName,
          maxLines: 2,
          overflow: TextOverflow.ellipsis,
          style: const TextStyle(color: Colors.white70, fontSize: 16),
        ),
      ),
    );
  }
}
