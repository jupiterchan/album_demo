import 'package:album/common/model/album_view_model.dart';
import 'package:get/get.dart';

class HomeState {
  late Rx<AlbumViewModel> albumViewModel;
  late Rx<AlbumViewModel> bookmarkAlbumViewModel;

  late Rx<bool> isLoading;
  late Rx<String> searchText;
  late Rx<int> pageIndex;

  HomeState() {
    ///Initialize variables
    albumViewModel = Rx(const AlbumViewModel(resultCount: 0, albums: []));
    bookmarkAlbumViewModel =
        Rx(const AlbumViewModel(resultCount: 0, albums: []));
    searchText = Rx("Jack Johnson");
    isLoading = Rx(false);
    pageIndex = Rx(0);
  }
}
