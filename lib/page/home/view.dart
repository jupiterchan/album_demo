import 'package:album/page/home/widget/album_grid_view.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'logic.dart';

class HomePage extends StatelessWidget {
  final logic = Get.put(HomeLogic());
  final state = Get.find<HomeLogic>().state;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          decoration: const BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topLeft,
                end: Alignment.centerRight,
                colors: [
                  Colors.yellowAccent,
                  Colors.white10,
                  Colors.white38,
                ]),
          ),
        ),
        Scaffold(
          backgroundColor: Colors.black54,
          appBar: AppBar(
            backgroundColor: Colors.transparent,
            elevation: 0,
            title: const Text(
              "Welcome",
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 24),
            ),
          ),
          body: Obx(() {
            return albumGridView(state.pageIndex.value);
          }),
          bottomNavigationBar: Obx(() {
            return BottomNavigationBar(
              items: const <BottomNavigationBarItem>[
                BottomNavigationBarItem(
                  icon: Icon(Icons.home),
                  label: "",
                ),
                BottomNavigationBarItem(
                  icon: Icon(Icons.favorite),
                  label: "",
                ),
              ],
              currentIndex: state.pageIndex.value,
              backgroundColor: Colors.transparent,
              selectedItemColor: Colors.white,
              unselectedItemColor: Colors.grey,
              onTap: logic.onTapBottomNavigationBar,
              showSelectedLabels: false,
              showUnselectedLabels: false,
            );
          }),
        ),
      ],
    );
  }

  Widget albumGridView(int index) {
    switch (index) {
      case 0:
        return Obx(() {
          return AlbumGridView(
            albumViewModel: state.albumViewModel.value,
            onBookmark: logic.onBookmark,
            gotoAlbumDetailsPage: logic.gotoAlbumDetailsPage,
          );
        });
      case 1:
        return Obx(() {
          return AlbumGridView(
            albumViewModel: state.bookmarkAlbumViewModel.value,
            onBookmark: logic.onBookmark,
            gotoAlbumDetailsPage: logic.gotoAlbumDetailsPage,
          );
        });
      default:
        return SizedBox();
    }
  }
}
