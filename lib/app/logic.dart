import 'package:get/get.dart';

import 'state.dart';

class AppLogic extends GetxController {
  final AppState state = AppState();
}
