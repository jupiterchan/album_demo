import 'package:album/page/album_details/view.dart';
import 'package:album/page/home/view.dart';
import 'package:album/utils/navigation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'logic.dart';

class AppPage extends StatelessWidget {
  final logic = Get.put(AppLogic());
  final state = Get.find<AppLogic>().state;

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Album Demo',
      getPages: [
        GetPage(name: Navigation.homePage, page: () => HomePage()),
        GetPage(
            name: Navigation.albumDetailsPage, page: () => AlbumDetailsPage()),
      ],
      theme: ThemeData(
        primarySwatch: Colors.indigo,
      ),
      home: HomePage(),
    );
  }
}
