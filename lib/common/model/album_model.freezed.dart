// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'album_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

AlbumModel _$AlbumModelFromJson(Map<String, dynamic> json) {
  return _AlbumModel.fromJson(json);
}

/// @nodoc
mixin _$AlbumModel {
  int get resultCount => throw _privateConstructorUsedError;
  List<Result> get results => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $AlbumModelCopyWith<AlbumModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AlbumModelCopyWith<$Res> {
  factory $AlbumModelCopyWith(
          AlbumModel value, $Res Function(AlbumModel) then) =
      _$AlbumModelCopyWithImpl<$Res>;
  $Res call({int resultCount, List<Result> results});
}

/// @nodoc
class _$AlbumModelCopyWithImpl<$Res> implements $AlbumModelCopyWith<$Res> {
  _$AlbumModelCopyWithImpl(this._value, this._then);

  final AlbumModel _value;
  // ignore: unused_field
  final $Res Function(AlbumModel) _then;

  @override
  $Res call({
    Object? resultCount = freezed,
    Object? results = freezed,
  }) {
    return _then(_value.copyWith(
      resultCount: resultCount == freezed
          ? _value.resultCount
          : resultCount // ignore: cast_nullable_to_non_nullable
              as int,
      results: results == freezed
          ? _value.results
          : results // ignore: cast_nullable_to_non_nullable
              as List<Result>,
    ));
  }
}

/// @nodoc
abstract class _$$_AlbumModelCopyWith<$Res>
    implements $AlbumModelCopyWith<$Res> {
  factory _$$_AlbumModelCopyWith(
          _$_AlbumModel value, $Res Function(_$_AlbumModel) then) =
      __$$_AlbumModelCopyWithImpl<$Res>;
  @override
  $Res call({int resultCount, List<Result> results});
}

/// @nodoc
class __$$_AlbumModelCopyWithImpl<$Res> extends _$AlbumModelCopyWithImpl<$Res>
    implements _$$_AlbumModelCopyWith<$Res> {
  __$$_AlbumModelCopyWithImpl(
      _$_AlbumModel _value, $Res Function(_$_AlbumModel) _then)
      : super(_value, (v) => _then(v as _$_AlbumModel));

  @override
  _$_AlbumModel get _value => super._value as _$_AlbumModel;

  @override
  $Res call({
    Object? resultCount = freezed,
    Object? results = freezed,
  }) {
    return _then(_$_AlbumModel(
      resultCount: resultCount == freezed
          ? _value.resultCount
          : resultCount // ignore: cast_nullable_to_non_nullable
              as int,
      results: results == freezed
          ? _value._results
          : results // ignore: cast_nullable_to_non_nullable
              as List<Result>,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_AlbumModel implements _AlbumModel {
  const _$_AlbumModel(
      {required this.resultCount, required final List<Result> results})
      : _results = results;

  factory _$_AlbumModel.fromJson(Map<String, dynamic> json) =>
      _$$_AlbumModelFromJson(json);

  @override
  final int resultCount;
  final List<Result> _results;
  @override
  List<Result> get results {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_results);
  }

  @override
  String toString() {
    return 'AlbumModel(resultCount: $resultCount, results: $results)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_AlbumModel &&
            const DeepCollectionEquality()
                .equals(other.resultCount, resultCount) &&
            const DeepCollectionEquality().equals(other._results, _results));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(resultCount),
      const DeepCollectionEquality().hash(_results));

  @JsonKey(ignore: true)
  @override
  _$$_AlbumModelCopyWith<_$_AlbumModel> get copyWith =>
      __$$_AlbumModelCopyWithImpl<_$_AlbumModel>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_AlbumModelToJson(this);
  }
}

abstract class _AlbumModel implements AlbumModel {
  const factory _AlbumModel(
      {required final int resultCount,
      required final List<Result> results}) = _$_AlbumModel;

  factory _AlbumModel.fromJson(Map<String, dynamic> json) =
      _$_AlbumModel.fromJson;

  @override
  int get resultCount => throw _privateConstructorUsedError;
  @override
  List<Result> get results => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$$_AlbumModelCopyWith<_$_AlbumModel> get copyWith =>
      throw _privateConstructorUsedError;
}

Result _$ResultFromJson(Map<String, dynamic> json) {
  return _Result.fromJson(json);
}

/// @nodoc
mixin _$Result {
  String get wrapperType => throw _privateConstructorUsedError;
  String get collectionType => throw _privateConstructorUsedError;
  int get artistId => throw _privateConstructorUsedError;
  int get collectionId => throw _privateConstructorUsedError;
  int? get amgArtistId => throw _privateConstructorUsedError;
  String get artistName => throw _privateConstructorUsedError;
  String get collectionName => throw _privateConstructorUsedError;
  String get collectionCensoredName => throw _privateConstructorUsedError;
  String? get artistViewUrl => throw _privateConstructorUsedError;
  String get collectionViewUrl => throw _privateConstructorUsedError;
  String get artworkUrl60 => throw _privateConstructorUsedError;
  String get artworkUrl100 => throw _privateConstructorUsedError;
  double? get collectionPrice => throw _privateConstructorUsedError;
  String get collectionExplicitness => throw _privateConstructorUsedError;
  int get trackCount => throw _privateConstructorUsedError;
  String get copyright => throw _privateConstructorUsedError;
  String get country => throw _privateConstructorUsedError;
  String get currency => throw _privateConstructorUsedError;
  String get releaseDate => throw _privateConstructorUsedError;
  String get primaryGenreName => throw _privateConstructorUsedError;
  String? get contentAdvisoryRating => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ResultCopyWith<Result> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ResultCopyWith<$Res> {
  factory $ResultCopyWith(Result value, $Res Function(Result) then) =
      _$ResultCopyWithImpl<$Res>;
  $Res call(
      {String wrapperType,
      String collectionType,
      int artistId,
      int collectionId,
      int? amgArtistId,
      String artistName,
      String collectionName,
      String collectionCensoredName,
      String? artistViewUrl,
      String collectionViewUrl,
      String artworkUrl60,
      String artworkUrl100,
      double? collectionPrice,
      String collectionExplicitness,
      int trackCount,
      String copyright,
      String country,
      String currency,
      String releaseDate,
      String primaryGenreName,
      String? contentAdvisoryRating});
}

/// @nodoc
class _$ResultCopyWithImpl<$Res> implements $ResultCopyWith<$Res> {
  _$ResultCopyWithImpl(this._value, this._then);

  final Result _value;
  // ignore: unused_field
  final $Res Function(Result) _then;

  @override
  $Res call({
    Object? wrapperType = freezed,
    Object? collectionType = freezed,
    Object? artistId = freezed,
    Object? collectionId = freezed,
    Object? amgArtistId = freezed,
    Object? artistName = freezed,
    Object? collectionName = freezed,
    Object? collectionCensoredName = freezed,
    Object? artistViewUrl = freezed,
    Object? collectionViewUrl = freezed,
    Object? artworkUrl60 = freezed,
    Object? artworkUrl100 = freezed,
    Object? collectionPrice = freezed,
    Object? collectionExplicitness = freezed,
    Object? trackCount = freezed,
    Object? copyright = freezed,
    Object? country = freezed,
    Object? currency = freezed,
    Object? releaseDate = freezed,
    Object? primaryGenreName = freezed,
    Object? contentAdvisoryRating = freezed,
  }) {
    return _then(_value.copyWith(
      wrapperType: wrapperType == freezed
          ? _value.wrapperType
          : wrapperType // ignore: cast_nullable_to_non_nullable
              as String,
      collectionType: collectionType == freezed
          ? _value.collectionType
          : collectionType // ignore: cast_nullable_to_non_nullable
              as String,
      artistId: artistId == freezed
          ? _value.artistId
          : artistId // ignore: cast_nullable_to_non_nullable
              as int,
      collectionId: collectionId == freezed
          ? _value.collectionId
          : collectionId // ignore: cast_nullable_to_non_nullable
              as int,
      amgArtistId: amgArtistId == freezed
          ? _value.amgArtistId
          : amgArtistId // ignore: cast_nullable_to_non_nullable
              as int?,
      artistName: artistName == freezed
          ? _value.artistName
          : artistName // ignore: cast_nullable_to_non_nullable
              as String,
      collectionName: collectionName == freezed
          ? _value.collectionName
          : collectionName // ignore: cast_nullable_to_non_nullable
              as String,
      collectionCensoredName: collectionCensoredName == freezed
          ? _value.collectionCensoredName
          : collectionCensoredName // ignore: cast_nullable_to_non_nullable
              as String,
      artistViewUrl: artistViewUrl == freezed
          ? _value.artistViewUrl
          : artistViewUrl // ignore: cast_nullable_to_non_nullable
              as String?,
      collectionViewUrl: collectionViewUrl == freezed
          ? _value.collectionViewUrl
          : collectionViewUrl // ignore: cast_nullable_to_non_nullable
              as String,
      artworkUrl60: artworkUrl60 == freezed
          ? _value.artworkUrl60
          : artworkUrl60 // ignore: cast_nullable_to_non_nullable
              as String,
      artworkUrl100: artworkUrl100 == freezed
          ? _value.artworkUrl100
          : artworkUrl100 // ignore: cast_nullable_to_non_nullable
              as String,
      collectionPrice: collectionPrice == freezed
          ? _value.collectionPrice
          : collectionPrice // ignore: cast_nullable_to_non_nullable
              as double?,
      collectionExplicitness: collectionExplicitness == freezed
          ? _value.collectionExplicitness
          : collectionExplicitness // ignore: cast_nullable_to_non_nullable
              as String,
      trackCount: trackCount == freezed
          ? _value.trackCount
          : trackCount // ignore: cast_nullable_to_non_nullable
              as int,
      copyright: copyright == freezed
          ? _value.copyright
          : copyright // ignore: cast_nullable_to_non_nullable
              as String,
      country: country == freezed
          ? _value.country
          : country // ignore: cast_nullable_to_non_nullable
              as String,
      currency: currency == freezed
          ? _value.currency
          : currency // ignore: cast_nullable_to_non_nullable
              as String,
      releaseDate: releaseDate == freezed
          ? _value.releaseDate
          : releaseDate // ignore: cast_nullable_to_non_nullable
              as String,
      primaryGenreName: primaryGenreName == freezed
          ? _value.primaryGenreName
          : primaryGenreName // ignore: cast_nullable_to_non_nullable
              as String,
      contentAdvisoryRating: contentAdvisoryRating == freezed
          ? _value.contentAdvisoryRating
          : contentAdvisoryRating // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
abstract class _$$_ResultCopyWith<$Res> implements $ResultCopyWith<$Res> {
  factory _$$_ResultCopyWith(_$_Result value, $Res Function(_$_Result) then) =
      __$$_ResultCopyWithImpl<$Res>;
  @override
  $Res call(
      {String wrapperType,
      String collectionType,
      int artistId,
      int collectionId,
      int? amgArtistId,
      String artistName,
      String collectionName,
      String collectionCensoredName,
      String? artistViewUrl,
      String collectionViewUrl,
      String artworkUrl60,
      String artworkUrl100,
      double? collectionPrice,
      String collectionExplicitness,
      int trackCount,
      String copyright,
      String country,
      String currency,
      String releaseDate,
      String primaryGenreName,
      String? contentAdvisoryRating});
}

/// @nodoc
class __$$_ResultCopyWithImpl<$Res> extends _$ResultCopyWithImpl<$Res>
    implements _$$_ResultCopyWith<$Res> {
  __$$_ResultCopyWithImpl(_$_Result _value, $Res Function(_$_Result) _then)
      : super(_value, (v) => _then(v as _$_Result));

  @override
  _$_Result get _value => super._value as _$_Result;

  @override
  $Res call({
    Object? wrapperType = freezed,
    Object? collectionType = freezed,
    Object? artistId = freezed,
    Object? collectionId = freezed,
    Object? amgArtistId = freezed,
    Object? artistName = freezed,
    Object? collectionName = freezed,
    Object? collectionCensoredName = freezed,
    Object? artistViewUrl = freezed,
    Object? collectionViewUrl = freezed,
    Object? artworkUrl60 = freezed,
    Object? artworkUrl100 = freezed,
    Object? collectionPrice = freezed,
    Object? collectionExplicitness = freezed,
    Object? trackCount = freezed,
    Object? copyright = freezed,
    Object? country = freezed,
    Object? currency = freezed,
    Object? releaseDate = freezed,
    Object? primaryGenreName = freezed,
    Object? contentAdvisoryRating = freezed,
  }) {
    return _then(_$_Result(
      wrapperType: wrapperType == freezed
          ? _value.wrapperType
          : wrapperType // ignore: cast_nullable_to_non_nullable
              as String,
      collectionType: collectionType == freezed
          ? _value.collectionType
          : collectionType // ignore: cast_nullable_to_non_nullable
              as String,
      artistId: artistId == freezed
          ? _value.artistId
          : artistId // ignore: cast_nullable_to_non_nullable
              as int,
      collectionId: collectionId == freezed
          ? _value.collectionId
          : collectionId // ignore: cast_nullable_to_non_nullable
              as int,
      amgArtistId: amgArtistId == freezed
          ? _value.amgArtistId
          : amgArtistId // ignore: cast_nullable_to_non_nullable
              as int?,
      artistName: artistName == freezed
          ? _value.artistName
          : artistName // ignore: cast_nullable_to_non_nullable
              as String,
      collectionName: collectionName == freezed
          ? _value.collectionName
          : collectionName // ignore: cast_nullable_to_non_nullable
              as String,
      collectionCensoredName: collectionCensoredName == freezed
          ? _value.collectionCensoredName
          : collectionCensoredName // ignore: cast_nullable_to_non_nullable
              as String,
      artistViewUrl: artistViewUrl == freezed
          ? _value.artistViewUrl
          : artistViewUrl // ignore: cast_nullable_to_non_nullable
              as String?,
      collectionViewUrl: collectionViewUrl == freezed
          ? _value.collectionViewUrl
          : collectionViewUrl // ignore: cast_nullable_to_non_nullable
              as String,
      artworkUrl60: artworkUrl60 == freezed
          ? _value.artworkUrl60
          : artworkUrl60 // ignore: cast_nullable_to_non_nullable
              as String,
      artworkUrl100: artworkUrl100 == freezed
          ? _value.artworkUrl100
          : artworkUrl100 // ignore: cast_nullable_to_non_nullable
              as String,
      collectionPrice: collectionPrice == freezed
          ? _value.collectionPrice
          : collectionPrice // ignore: cast_nullable_to_non_nullable
              as double?,
      collectionExplicitness: collectionExplicitness == freezed
          ? _value.collectionExplicitness
          : collectionExplicitness // ignore: cast_nullable_to_non_nullable
              as String,
      trackCount: trackCount == freezed
          ? _value.trackCount
          : trackCount // ignore: cast_nullable_to_non_nullable
              as int,
      copyright: copyright == freezed
          ? _value.copyright
          : copyright // ignore: cast_nullable_to_non_nullable
              as String,
      country: country == freezed
          ? _value.country
          : country // ignore: cast_nullable_to_non_nullable
              as String,
      currency: currency == freezed
          ? _value.currency
          : currency // ignore: cast_nullable_to_non_nullable
              as String,
      releaseDate: releaseDate == freezed
          ? _value.releaseDate
          : releaseDate // ignore: cast_nullable_to_non_nullable
              as String,
      primaryGenreName: primaryGenreName == freezed
          ? _value.primaryGenreName
          : primaryGenreName // ignore: cast_nullable_to_non_nullable
              as String,
      contentAdvisoryRating: contentAdvisoryRating == freezed
          ? _value.contentAdvisoryRating
          : contentAdvisoryRating // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_Result implements _Result {
  const _$_Result(
      {required this.wrapperType,
      required this.collectionType,
      required this.artistId,
      required this.collectionId,
      this.amgArtistId,
      required this.artistName,
      required this.collectionName,
      required this.collectionCensoredName,
      this.artistViewUrl,
      required this.collectionViewUrl,
      required this.artworkUrl60,
      required this.artworkUrl100,
      this.collectionPrice,
      required this.collectionExplicitness,
      required this.trackCount,
      required this.copyright,
      required this.country,
      required this.currency,
      required this.releaseDate,
      required this.primaryGenreName,
      this.contentAdvisoryRating});

  factory _$_Result.fromJson(Map<String, dynamic> json) =>
      _$$_ResultFromJson(json);

  @override
  final String wrapperType;
  @override
  final String collectionType;
  @override
  final int artistId;
  @override
  final int collectionId;
  @override
  final int? amgArtistId;
  @override
  final String artistName;
  @override
  final String collectionName;
  @override
  final String collectionCensoredName;
  @override
  final String? artistViewUrl;
  @override
  final String collectionViewUrl;
  @override
  final String artworkUrl60;
  @override
  final String artworkUrl100;
  @override
  final double? collectionPrice;
  @override
  final String collectionExplicitness;
  @override
  final int trackCount;
  @override
  final String copyright;
  @override
  final String country;
  @override
  final String currency;
  @override
  final String releaseDate;
  @override
  final String primaryGenreName;
  @override
  final String? contentAdvisoryRating;

  @override
  String toString() {
    return 'Result(wrapperType: $wrapperType, collectionType: $collectionType, artistId: $artistId, collectionId: $collectionId, amgArtistId: $amgArtistId, artistName: $artistName, collectionName: $collectionName, collectionCensoredName: $collectionCensoredName, artistViewUrl: $artistViewUrl, collectionViewUrl: $collectionViewUrl, artworkUrl60: $artworkUrl60, artworkUrl100: $artworkUrl100, collectionPrice: $collectionPrice, collectionExplicitness: $collectionExplicitness, trackCount: $trackCount, copyright: $copyright, country: $country, currency: $currency, releaseDate: $releaseDate, primaryGenreName: $primaryGenreName, contentAdvisoryRating: $contentAdvisoryRating)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_Result &&
            const DeepCollectionEquality()
                .equals(other.wrapperType, wrapperType) &&
            const DeepCollectionEquality()
                .equals(other.collectionType, collectionType) &&
            const DeepCollectionEquality().equals(other.artistId, artistId) &&
            const DeepCollectionEquality()
                .equals(other.collectionId, collectionId) &&
            const DeepCollectionEquality()
                .equals(other.amgArtistId, amgArtistId) &&
            const DeepCollectionEquality()
                .equals(other.artistName, artistName) &&
            const DeepCollectionEquality()
                .equals(other.collectionName, collectionName) &&
            const DeepCollectionEquality()
                .equals(other.collectionCensoredName, collectionCensoredName) &&
            const DeepCollectionEquality()
                .equals(other.artistViewUrl, artistViewUrl) &&
            const DeepCollectionEquality()
                .equals(other.collectionViewUrl, collectionViewUrl) &&
            const DeepCollectionEquality()
                .equals(other.artworkUrl60, artworkUrl60) &&
            const DeepCollectionEquality()
                .equals(other.artworkUrl100, artworkUrl100) &&
            const DeepCollectionEquality()
                .equals(other.collectionPrice, collectionPrice) &&
            const DeepCollectionEquality()
                .equals(other.collectionExplicitness, collectionExplicitness) &&
            const DeepCollectionEquality()
                .equals(other.trackCount, trackCount) &&
            const DeepCollectionEquality().equals(other.copyright, copyright) &&
            const DeepCollectionEquality().equals(other.country, country) &&
            const DeepCollectionEquality().equals(other.currency, currency) &&
            const DeepCollectionEquality()
                .equals(other.releaseDate, releaseDate) &&
            const DeepCollectionEquality()
                .equals(other.primaryGenreName, primaryGenreName) &&
            const DeepCollectionEquality()
                .equals(other.contentAdvisoryRating, contentAdvisoryRating));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hashAll([
        runtimeType,
        const DeepCollectionEquality().hash(wrapperType),
        const DeepCollectionEquality().hash(collectionType),
        const DeepCollectionEquality().hash(artistId),
        const DeepCollectionEquality().hash(collectionId),
        const DeepCollectionEquality().hash(amgArtistId),
        const DeepCollectionEquality().hash(artistName),
        const DeepCollectionEquality().hash(collectionName),
        const DeepCollectionEquality().hash(collectionCensoredName),
        const DeepCollectionEquality().hash(artistViewUrl),
        const DeepCollectionEquality().hash(collectionViewUrl),
        const DeepCollectionEquality().hash(artworkUrl60),
        const DeepCollectionEquality().hash(artworkUrl100),
        const DeepCollectionEquality().hash(collectionPrice),
        const DeepCollectionEquality().hash(collectionExplicitness),
        const DeepCollectionEquality().hash(trackCount),
        const DeepCollectionEquality().hash(copyright),
        const DeepCollectionEquality().hash(country),
        const DeepCollectionEquality().hash(currency),
        const DeepCollectionEquality().hash(releaseDate),
        const DeepCollectionEquality().hash(primaryGenreName),
        const DeepCollectionEquality().hash(contentAdvisoryRating)
      ]);

  @JsonKey(ignore: true)
  @override
  _$$_ResultCopyWith<_$_Result> get copyWith =>
      __$$_ResultCopyWithImpl<_$_Result>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_ResultToJson(this);
  }
}

abstract class _Result implements Result {
  const factory _Result(
      {required final String wrapperType,
      required final String collectionType,
      required final int artistId,
      required final int collectionId,
      final int? amgArtistId,
      required final String artistName,
      required final String collectionName,
      required final String collectionCensoredName,
      final String? artistViewUrl,
      required final String collectionViewUrl,
      required final String artworkUrl60,
      required final String artworkUrl100,
      final double? collectionPrice,
      required final String collectionExplicitness,
      required final int trackCount,
      required final String copyright,
      required final String country,
      required final String currency,
      required final String releaseDate,
      required final String primaryGenreName,
      final String? contentAdvisoryRating}) = _$_Result;

  factory _Result.fromJson(Map<String, dynamic> json) = _$_Result.fromJson;

  @override
  String get wrapperType => throw _privateConstructorUsedError;
  @override
  String get collectionType => throw _privateConstructorUsedError;
  @override
  int get artistId => throw _privateConstructorUsedError;
  @override
  int get collectionId => throw _privateConstructorUsedError;
  @override
  int? get amgArtistId => throw _privateConstructorUsedError;
  @override
  String get artistName => throw _privateConstructorUsedError;
  @override
  String get collectionName => throw _privateConstructorUsedError;
  @override
  String get collectionCensoredName => throw _privateConstructorUsedError;
  @override
  String? get artistViewUrl => throw _privateConstructorUsedError;
  @override
  String get collectionViewUrl => throw _privateConstructorUsedError;
  @override
  String get artworkUrl60 => throw _privateConstructorUsedError;
  @override
  String get artworkUrl100 => throw _privateConstructorUsedError;
  @override
  double? get collectionPrice => throw _privateConstructorUsedError;
  @override
  String get collectionExplicitness => throw _privateConstructorUsedError;
  @override
  int get trackCount => throw _privateConstructorUsedError;
  @override
  String get copyright => throw _privateConstructorUsedError;
  @override
  String get country => throw _privateConstructorUsedError;
  @override
  String get currency => throw _privateConstructorUsedError;
  @override
  String get releaseDate => throw _privateConstructorUsedError;
  @override
  String get primaryGenreName => throw _privateConstructorUsedError;
  @override
  String? get contentAdvisoryRating => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$$_ResultCopyWith<_$_Result> get copyWith =>
      throw _privateConstructorUsedError;
}
