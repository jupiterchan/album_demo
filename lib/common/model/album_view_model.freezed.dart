// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'album_view_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

AlbumViewModel _$AlbumViewModelFromJson(Map<String, dynamic> json) {
  return _AlbumViewModel.fromJson(json);
}

/// @nodoc
mixin _$AlbumViewModel {
  int get resultCount => throw _privateConstructorUsedError;
  List<Album> get albums => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $AlbumViewModelCopyWith<AlbumViewModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AlbumViewModelCopyWith<$Res> {
  factory $AlbumViewModelCopyWith(
          AlbumViewModel value, $Res Function(AlbumViewModel) then) =
      _$AlbumViewModelCopyWithImpl<$Res>;
  $Res call({int resultCount, List<Album> albums});
}

/// @nodoc
class _$AlbumViewModelCopyWithImpl<$Res>
    implements $AlbumViewModelCopyWith<$Res> {
  _$AlbumViewModelCopyWithImpl(this._value, this._then);

  final AlbumViewModel _value;
  // ignore: unused_field
  final $Res Function(AlbumViewModel) _then;

  @override
  $Res call({
    Object? resultCount = freezed,
    Object? albums = freezed,
  }) {
    return _then(_value.copyWith(
      resultCount: resultCount == freezed
          ? _value.resultCount
          : resultCount // ignore: cast_nullable_to_non_nullable
              as int,
      albums: albums == freezed
          ? _value.albums
          : albums // ignore: cast_nullable_to_non_nullable
              as List<Album>,
    ));
  }
}

/// @nodoc
abstract class _$$_AlbumViewModelCopyWith<$Res>
    implements $AlbumViewModelCopyWith<$Res> {
  factory _$$_AlbumViewModelCopyWith(
          _$_AlbumViewModel value, $Res Function(_$_AlbumViewModel) then) =
      __$$_AlbumViewModelCopyWithImpl<$Res>;
  @override
  $Res call({int resultCount, List<Album> albums});
}

/// @nodoc
class __$$_AlbumViewModelCopyWithImpl<$Res>
    extends _$AlbumViewModelCopyWithImpl<$Res>
    implements _$$_AlbumViewModelCopyWith<$Res> {
  __$$_AlbumViewModelCopyWithImpl(
      _$_AlbumViewModel _value, $Res Function(_$_AlbumViewModel) _then)
      : super(_value, (v) => _then(v as _$_AlbumViewModel));

  @override
  _$_AlbumViewModel get _value => super._value as _$_AlbumViewModel;

  @override
  $Res call({
    Object? resultCount = freezed,
    Object? albums = freezed,
  }) {
    return _then(_$_AlbumViewModel(
      resultCount: resultCount == freezed
          ? _value.resultCount
          : resultCount // ignore: cast_nullable_to_non_nullable
              as int,
      albums: albums == freezed
          ? _value._albums
          : albums // ignore: cast_nullable_to_non_nullable
              as List<Album>,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_AlbumViewModel implements _AlbumViewModel {
  const _$_AlbumViewModel(
      {required this.resultCount, required final List<Album> albums})
      : _albums = albums;

  factory _$_AlbumViewModel.fromJson(Map<String, dynamic> json) =>
      _$$_AlbumViewModelFromJson(json);

  @override
  final int resultCount;
  final List<Album> _albums;
  @override
  List<Album> get albums {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_albums);
  }

  @override
  String toString() {
    return 'AlbumViewModel(resultCount: $resultCount, albums: $albums)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_AlbumViewModel &&
            const DeepCollectionEquality()
                .equals(other.resultCount, resultCount) &&
            const DeepCollectionEquality().equals(other._albums, _albums));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(resultCount),
      const DeepCollectionEquality().hash(_albums));

  @JsonKey(ignore: true)
  @override
  _$$_AlbumViewModelCopyWith<_$_AlbumViewModel> get copyWith =>
      __$$_AlbumViewModelCopyWithImpl<_$_AlbumViewModel>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_AlbumViewModelToJson(this);
  }
}

abstract class _AlbumViewModel implements AlbumViewModel {
  const factory _AlbumViewModel(
      {required final int resultCount,
      required final List<Album> albums}) = _$_AlbumViewModel;

  factory _AlbumViewModel.fromJson(Map<String, dynamic> json) =
      _$_AlbumViewModel.fromJson;

  @override
  int get resultCount => throw _privateConstructorUsedError;
  @override
  List<Album> get albums => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$$_AlbumViewModelCopyWith<_$_AlbumViewModel> get copyWith =>
      throw _privateConstructorUsedError;
}

Album _$AlbumFromJson(Map<String, dynamic> json) {
  return _Album.fromJson(json);
}

/// @nodoc
mixin _$Album {
  int get collectionId => throw _privateConstructorUsedError;
  String get artistName => throw _privateConstructorUsedError;
  String get collectionName => throw _privateConstructorUsedError;
  String? get artistViewUrl => throw _privateConstructorUsedError;
  String get collectionViewUrl => throw _privateConstructorUsedError;
  String get artworkUrl100 => throw _privateConstructorUsedError;
  double? get collectionPrice => throw _privateConstructorUsedError;
  bool get bookmark => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $AlbumCopyWith<Album> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AlbumCopyWith<$Res> {
  factory $AlbumCopyWith(Album value, $Res Function(Album) then) =
      _$AlbumCopyWithImpl<$Res>;
  $Res call(
      {int collectionId,
      String artistName,
      String collectionName,
      String? artistViewUrl,
      String collectionViewUrl,
      String artworkUrl100,
      double? collectionPrice,
      bool bookmark});
}

/// @nodoc
class _$AlbumCopyWithImpl<$Res> implements $AlbumCopyWith<$Res> {
  _$AlbumCopyWithImpl(this._value, this._then);

  final Album _value;
  // ignore: unused_field
  final $Res Function(Album) _then;

  @override
  $Res call({
    Object? collectionId = freezed,
    Object? artistName = freezed,
    Object? collectionName = freezed,
    Object? artistViewUrl = freezed,
    Object? collectionViewUrl = freezed,
    Object? artworkUrl100 = freezed,
    Object? collectionPrice = freezed,
    Object? bookmark = freezed,
  }) {
    return _then(_value.copyWith(
      collectionId: collectionId == freezed
          ? _value.collectionId
          : collectionId // ignore: cast_nullable_to_non_nullable
              as int,
      artistName: artistName == freezed
          ? _value.artistName
          : artistName // ignore: cast_nullable_to_non_nullable
              as String,
      collectionName: collectionName == freezed
          ? _value.collectionName
          : collectionName // ignore: cast_nullable_to_non_nullable
              as String,
      artistViewUrl: artistViewUrl == freezed
          ? _value.artistViewUrl
          : artistViewUrl // ignore: cast_nullable_to_non_nullable
              as String?,
      collectionViewUrl: collectionViewUrl == freezed
          ? _value.collectionViewUrl
          : collectionViewUrl // ignore: cast_nullable_to_non_nullable
              as String,
      artworkUrl100: artworkUrl100 == freezed
          ? _value.artworkUrl100
          : artworkUrl100 // ignore: cast_nullable_to_non_nullable
              as String,
      collectionPrice: collectionPrice == freezed
          ? _value.collectionPrice
          : collectionPrice // ignore: cast_nullable_to_non_nullable
              as double?,
      bookmark: bookmark == freezed
          ? _value.bookmark
          : bookmark // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc
abstract class _$$_AlbumCopyWith<$Res> implements $AlbumCopyWith<$Res> {
  factory _$$_AlbumCopyWith(_$_Album value, $Res Function(_$_Album) then) =
      __$$_AlbumCopyWithImpl<$Res>;
  @override
  $Res call(
      {int collectionId,
      String artistName,
      String collectionName,
      String? artistViewUrl,
      String collectionViewUrl,
      String artworkUrl100,
      double? collectionPrice,
      bool bookmark});
}

/// @nodoc
class __$$_AlbumCopyWithImpl<$Res> extends _$AlbumCopyWithImpl<$Res>
    implements _$$_AlbumCopyWith<$Res> {
  __$$_AlbumCopyWithImpl(_$_Album _value, $Res Function(_$_Album) _then)
      : super(_value, (v) => _then(v as _$_Album));

  @override
  _$_Album get _value => super._value as _$_Album;

  @override
  $Res call({
    Object? collectionId = freezed,
    Object? artistName = freezed,
    Object? collectionName = freezed,
    Object? artistViewUrl = freezed,
    Object? collectionViewUrl = freezed,
    Object? artworkUrl100 = freezed,
    Object? collectionPrice = freezed,
    Object? bookmark = freezed,
  }) {
    return _then(_$_Album(
      collectionId: collectionId == freezed
          ? _value.collectionId
          : collectionId // ignore: cast_nullable_to_non_nullable
              as int,
      artistName: artistName == freezed
          ? _value.artistName
          : artistName // ignore: cast_nullable_to_non_nullable
              as String,
      collectionName: collectionName == freezed
          ? _value.collectionName
          : collectionName // ignore: cast_nullable_to_non_nullable
              as String,
      artistViewUrl: artistViewUrl == freezed
          ? _value.artistViewUrl
          : artistViewUrl // ignore: cast_nullable_to_non_nullable
              as String?,
      collectionViewUrl: collectionViewUrl == freezed
          ? _value.collectionViewUrl
          : collectionViewUrl // ignore: cast_nullable_to_non_nullable
              as String,
      artworkUrl100: artworkUrl100 == freezed
          ? _value.artworkUrl100
          : artworkUrl100 // ignore: cast_nullable_to_non_nullable
              as String,
      collectionPrice: collectionPrice == freezed
          ? _value.collectionPrice
          : collectionPrice // ignore: cast_nullable_to_non_nullable
              as double?,
      bookmark: bookmark == freezed
          ? _value.bookmark
          : bookmark // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_Album implements _Album {
  const _$_Album(
      {required this.collectionId,
      required this.artistName,
      required this.collectionName,
      this.artistViewUrl,
      required this.collectionViewUrl,
      required this.artworkUrl100,
      this.collectionPrice,
      this.bookmark = false});

  factory _$_Album.fromJson(Map<String, dynamic> json) =>
      _$$_AlbumFromJson(json);

  @override
  final int collectionId;
  @override
  final String artistName;
  @override
  final String collectionName;
  @override
  final String? artistViewUrl;
  @override
  final String collectionViewUrl;
  @override
  final String artworkUrl100;
  @override
  final double? collectionPrice;
  @override
  @JsonKey()
  final bool bookmark;

  @override
  String toString() {
    return 'Album(collectionId: $collectionId, artistName: $artistName, collectionName: $collectionName, artistViewUrl: $artistViewUrl, collectionViewUrl: $collectionViewUrl, artworkUrl100: $artworkUrl100, collectionPrice: $collectionPrice, bookmark: $bookmark)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_Album &&
            const DeepCollectionEquality()
                .equals(other.collectionId, collectionId) &&
            const DeepCollectionEquality()
                .equals(other.artistName, artistName) &&
            const DeepCollectionEquality()
                .equals(other.collectionName, collectionName) &&
            const DeepCollectionEquality()
                .equals(other.artistViewUrl, artistViewUrl) &&
            const DeepCollectionEquality()
                .equals(other.collectionViewUrl, collectionViewUrl) &&
            const DeepCollectionEquality()
                .equals(other.artworkUrl100, artworkUrl100) &&
            const DeepCollectionEquality()
                .equals(other.collectionPrice, collectionPrice) &&
            const DeepCollectionEquality().equals(other.bookmark, bookmark));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(collectionId),
      const DeepCollectionEquality().hash(artistName),
      const DeepCollectionEquality().hash(collectionName),
      const DeepCollectionEquality().hash(artistViewUrl),
      const DeepCollectionEquality().hash(collectionViewUrl),
      const DeepCollectionEquality().hash(artworkUrl100),
      const DeepCollectionEquality().hash(collectionPrice),
      const DeepCollectionEquality().hash(bookmark));

  @JsonKey(ignore: true)
  @override
  _$$_AlbumCopyWith<_$_Album> get copyWith =>
      __$$_AlbumCopyWithImpl<_$_Album>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_AlbumToJson(this);
  }
}

abstract class _Album implements Album {
  const factory _Album(
      {required final int collectionId,
      required final String artistName,
      required final String collectionName,
      final String? artistViewUrl,
      required final String collectionViewUrl,
      required final String artworkUrl100,
      final double? collectionPrice,
      final bool bookmark}) = _$_Album;

  factory _Album.fromJson(Map<String, dynamic> json) = _$_Album.fromJson;

  @override
  int get collectionId => throw _privateConstructorUsedError;
  @override
  String get artistName => throw _privateConstructorUsedError;
  @override
  String get collectionName => throw _privateConstructorUsedError;
  @override
  String? get artistViewUrl => throw _privateConstructorUsedError;
  @override
  String get collectionViewUrl => throw _privateConstructorUsedError;
  @override
  String get artworkUrl100 => throw _privateConstructorUsedError;
  @override
  double? get collectionPrice => throw _privateConstructorUsedError;
  @override
  bool get bookmark => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$$_AlbumCopyWith<_$_Album> get copyWith =>
      throw _privateConstructorUsedError;
}
