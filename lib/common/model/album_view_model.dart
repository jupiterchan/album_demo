// To parse this JSON data, do
//
//     final albumViewModel = albumViewModelFromJson(jsonString);

import 'package:meta/meta.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'dart:convert';

part 'album_view_model.freezed.dart';
part 'album_view_model.g.dart';

AlbumViewModel albumViewModelFromJson(String str) =>
    AlbumViewModel.fromJson(json.decode(str));

String albumViewModelToJson(AlbumViewModel data) => json.encode(data.toJson());

@freezed
abstract class AlbumViewModel with _$AlbumViewModel {
  const factory AlbumViewModel({
    required int resultCount,
    required List<Album> albums,
  }) = _AlbumViewModel;

  factory AlbumViewModel.fromJson(Map<String, dynamic> json) =>
      _$AlbumViewModelFromJson(json);
}

@freezed
abstract class Album with _$Album {
  const factory Album({
    required int collectionId,
    required String artistName,
    required String collectionName,
    String? artistViewUrl,
    required String collectionViewUrl,
    required String artworkUrl100,
    double? collectionPrice,
    @Default(false) bool bookmark,
  }) = _Album;

  factory Album.fromJson(Map<String, dynamic> json) => _$AlbumFromJson(json);
}
