// To parse this JSON data, do
//
//     final albumModel = albumModelFromJson(jsonString);

import 'package:meta/meta.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'dart:convert';

part 'album_model.freezed.dart';
part 'album_model.g.dart';

AlbumModel albumModelFromJson(String str) =>
    AlbumModel.fromJson(json.decode(str));

String albumModelToJson(AlbumModel data) => json.encode(data.toJson());

@freezed
abstract class AlbumModel with _$AlbumModel {
  const factory AlbumModel({
    required int resultCount,
    required List<Result> results,
  }) = _AlbumModel;

  factory AlbumModel.fromJson(Map<String, dynamic> json) =>
      _$AlbumModelFromJson(json);
}

@freezed
abstract class Result with _$Result {
  const factory Result({
    required String wrapperType,
    required String collectionType,
    required int artistId,
    required int collectionId,
    int? amgArtistId,
    required String artistName,
    required String collectionName,
    required String collectionCensoredName,
    String? artistViewUrl,
    required String collectionViewUrl,
    required String artworkUrl60,
    required String artworkUrl100,
    double? collectionPrice,
    required String collectionExplicitness,
    required int trackCount,
    required String copyright,
    required String country,
    required String currency,
    required String releaseDate,
    required String primaryGenreName,
    String? contentAdvisoryRating,
  }) = _Result;

  factory Result.fromJson(Map<String, dynamic> json) => _$ResultFromJson(json);
}
