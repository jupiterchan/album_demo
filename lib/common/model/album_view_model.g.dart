// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'album_view_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_AlbumViewModel _$$_AlbumViewModelFromJson(Map<String, dynamic> json) =>
    _$_AlbumViewModel(
      resultCount: json['resultCount'] as int,
      albums: (json['albums'] as List<dynamic>)
          .map((e) => Album.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$$_AlbumViewModelToJson(_$_AlbumViewModel instance) =>
    <String, dynamic>{
      'resultCount': instance.resultCount,
      'albums': instance.albums,
    };

_$_Album _$$_AlbumFromJson(Map<String, dynamic> json) => _$_Album(
      collectionId: json['collectionId'] as int,
      artistName: json['artistName'] as String,
      collectionName: json['collectionName'] as String,
      artistViewUrl: json['artistViewUrl'] as String?,
      collectionViewUrl: json['collectionViewUrl'] as String,
      artworkUrl100: json['artworkUrl100'] as String,
      collectionPrice: (json['collectionPrice'] as num?)?.toDouble(),
      bookmark: json['bookmark'] as bool? ?? false,
    );

Map<String, dynamic> _$$_AlbumToJson(_$_Album instance) => <String, dynamic>{
      'collectionId': instance.collectionId,
      'artistName': instance.artistName,
      'collectionName': instance.collectionName,
      'artistViewUrl': instance.artistViewUrl,
      'collectionViewUrl': instance.collectionViewUrl,
      'artworkUrl100': instance.artworkUrl100,
      'collectionPrice': instance.collectionPrice,
      'bookmark': instance.bookmark,
    };
